/* 

Copyright (C) 2022 landscape contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:flutter_acrylic/flutter_acrylic.dart';
import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:landscape/widgets/room_list.dart';
import 'package:landscape/widgets/message_view.dart';
import 'package:landscape/api/client_handler.dart';
import 'package:landscape/api/login_handler.dart';
import 'package:landscape/widgets/send_message.dart';

ClientHandler clientHandler = ClientHandler();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Window.initialize();
  await Window.setEffect(
    effect: WindowEffect.transparent,
    color: const Color(0x00FFFFFF),
  );
  runApp(const LandscapeApp());
  doWhenWindowReady(
    () {
      const initialSize = Size(800, 600);
      appWindow.minSize = initialSize;
      appWindow.size = initialSize;
      appWindow.alignment = Alignment.center;
      appWindow.show();
    },
  );
  // LoginHandler loginHandler = LoginHandler(
  //   homeserver: "https://matrix-client.matrix.org",
  //   username: "test",
  //   password: "test",
  //   client: clientHandler.client,
  // );
  // loginHandler.login();
}

class LandscapeApp extends StatefulWidget {
  const LandscapeApp({Key? key}) : super(key: key);

  @override
  State<LandscapeApp> createState() => _LandscapeAppState();
}

class _LandscapeAppState extends State<LandscapeApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'landscape',
      theme: ThemeData(
        useMaterial3: true,
        brightness: Brightness.dark,
        colorScheme: const ColorScheme(
            brightness: Brightness.dark,
            background: Color(0xFF181D22),
            onBackground: Color(0xFFEAEAEA),
            primary: Color(0xFF181D22),
            onPrimary: Color(0xFFE4F5FF),
            secondary: Color(0xFF212332),
            onSecondary: Color.fromARGB(255, 255, 255, 255),
            tertiary: Color(0xFF00FFC3),
            onTertiary: Color(0xFF030F0D),
            error: Color(0xFFEF3557),
            onError: Color(0xFFFFFFFF),
            surface: Color(0xFF181D22),
            onSurface: Color(0xFFBDCEDF)),
      ),
      home: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: const HomePage(title: "landscape")),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  RoomList? roomList;
  MessageView? messageView;
  SendMessage? sendMessage;
  @override
  void initState() {
    super.initState();

    messageView?.client = clientHandler.client;

    roomList = RoomList(
      client: clientHandler.client,
      onRoomSelected: () {
        setState(
          () {
            messageView = MessageView(
              client: clientHandler.client,
              roomIndex: roomList!.selectedIndex,
            );
            sendMessage = SendMessage(
              clientHandler: clientHandler,
              roomIndex: roomList!.selectedIndex,
            );
          },
        );
      },
    );
    messageView = MessageView(
      client: clientHandler.client,
      roomIndex: roomList!.selectedIndex,
    );
    sendMessage = SendMessage(
        clientHandler: clientHandler, roomIndex: roomList!.selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            constraints: const BoxConstraints(
              minWidth: 96,
              maxWidth: 128,
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.secondary,
              border: const Border(
                right: BorderSide(
                  color: Color.fromARGB(255, 80, 84, 112),
                  width: 1,
                ),
              ),
            ),
            child: Column(
              children: const [],
            ),
          ),
          Container(
            constraints: const BoxConstraints(
              minWidth: 384,
              maxWidth: 512,
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.secondary,
              border: const Border(
                right: BorderSide(
                  color: Color.fromARGB(255, 80, 84, 112),
                  width: 1,
                ),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Container(
                    margin: const EdgeInsets.only(
                      left: 48,
                      top: 32,
                    ),
                    child: Text(
                      "Rooms",
                      style: GoogleFonts.openSans(
                        fontSize: 36,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                ),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Container(
                    margin: const EdgeInsets.only(
                      left: 48,
                    ),
                    child: Text(
                      "Find more...",
                      style: GoogleFonts.openSans(
                        color: Theme.of(context).colorScheme.onSecondary,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: Container(
                    margin: const EdgeInsets.only(
                      top: 32,
                      bottom: 32,
                    ),
                    child:
                        roomList ?? const CircularProgressIndicator.adaptive(),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: Container(
              alignment: Alignment.topCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  WindowTitleBarBox(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(child: MoveWindow()),
                        MinimizeWindowButton(
                          colors: WindowButtonColors(
                            mouseOver: Theme.of(context).colorScheme.secondary,
                            iconNormal:
                                Theme.of(context).colorScheme.onBackground,
                          ),
                        ),
                        MaximizeWindowButton(
                          colors: WindowButtonColors(
                            mouseOver: Theme.of(context).colorScheme.secondary,
                            iconNormal:
                                Theme.of(context).colorScheme.onBackground,
                          ),
                        ),
                        CloseWindowButton(
                          colors: WindowButtonColors(
                            mouseOver: Theme.of(context).colorScheme.error,
                            iconNormal:
                                Theme.of(context).colorScheme.onBackground,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: messageView!,
                  ),
                  sendMessage!,
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
