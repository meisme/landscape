/* 

Copyright (C) 2022 landscape contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:matrix/matrix.dart';

class LoginHandler {
  String? homeserver = "https://matrix-client.matrix.org";
  final String username, password;
  final Client client;
  LoginHandler({
    this.homeserver,
    required this.username,
    required this.password,
    required this.client,
  });
  Future<String> login() async {
    String response = "Empty Response.";
    await client.checkHomeserver(
        Uri.parse(homeserver ?? "https://matrix-client.matrix.org"));
    await client
        .login(
          LoginType.mLoginPassword,
          identifier: AuthenticationUserIdentifier(user: username),
          password: password,
        )
        .then(
          (value) => response = "Logged in",
        )
        .onError((error, stackTrace) => response = stackTrace.toString());
    return response;
  }
}
