/* 

Copyright (C) 2022 landscape contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:google_fonts/google_fonts.dart';
import 'package:landscape/main.dart';
import 'package:matrix/matrix.dart';

class MessageView extends StatefulWidget {
  Client client;
  int roomIndex;
  MessageView({
    Key? key,
    required this.client,
    required this.roomIndex,
  }) : super(key: key);

  @override
  MessageViewState createState() => MessageViewState();
}

class MessageViewState extends State<MessageView> {
  Future<Timeline> getTimeline() async {
    await Future.doWhile(
      () => Future.delayed(
        const Duration(milliseconds: 5),
      ).then(
        (_) {
          return widget.client.rooms.isEmpty;
        },
      ),
    );
    return await widget.client.rooms[widget.roomIndex].getTimeline();
  }

  @override
  Widget build(BuildContext context) {
    final Future<Timeline> timelineFuture = getTimeline();
    final ScrollController scrollController = ScrollController();

    return StreamBuilder(
      stream: clientHandler.client.onSync.stream,
      builder: (context, _) {
        return FutureBuilder<Timeline>(
          future: timelineFuture,
          builder: (context, snapshot) {
            final timeline = snapshot.data;
            if (timeline == null) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            scrollController.addListener(
              () {
                if (scrollController.position.atEdge &&
                    scrollController.position.pixels != 0) {
                  timeline.requestHistory();
                }
              },
            );
            return ListView.builder(
              reverse: true,
              controller: scrollController,
              itemCount: timeline.events.length,
              itemBuilder: (context, i) => Align(
                alignment: Alignment.topLeft,
                child: Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: FutureBuilder<Profile>(
                    future: clientHandler.client
                        .getProfileFromUserId(timeline.events[i].senderId),
                    builder: (context, snapshot) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment:
                            snapshot.data?.userId == clientHandler.client.userID
                                ? MainAxisAlignment.end
                                : MainAxisAlignment.start,
                        children: [
                          Stack(
                            children: [
                              Align(
                                child: AnimatedContainer(
                                  duration: const Duration(milliseconds: 300),
                                  padding: const EdgeInsets.all(16),
                                  margin: const EdgeInsets.only(
                                    top: 56,
                                    right: 32,
                                    left: 32,
                                  ),
                                  constraints: const BoxConstraints(
                                    maxWidth: 512,
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: const Color(0xFF18324D),
                                  ),
                                  child: MarkdownBody(
                                    extensionSet:
                                        md.ExtensionSet.gitHubFlavored,
                                    data: (timeline.events[i].content.entries
                                                .isNotEmpty &&
                                            timeline.events[i].content.entries
                                                .first.value is String
                                        ? timeline.events[i].content.entries
                                            .first.value
                                        : "Failed to load message of type (${timeline.events[i].body})"),
                                  ),
                                ),
                              ),
                              CircleAvatar(
                                radius: 36,
                                backgroundColor:
                                    Theme.of(context).colorScheme.primary,
                                child: ClipOval(
                                  child: CachedNetworkImage(
                                    height: 56,
                                    width: 56,
                                    imageUrl: (snapshot.data?.avatarUrl ??
                                            Uri(path: "invalid"))
                                        .getThumbnail(widget.client,
                                            width: 128, height: 128)
                                        .toString(),
                                    errorWidget: (context, url, error) =>
                                        Container(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      child: Center(
                                        child: Text(
                                          (snapshot.data?.displayName ??
                                                  "NA".toString())
                                              .substring(0, 1),
                                          style: GoogleFonts.openSans(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .onSecondary,
                                            fontSize: 24,
                                            fontWeight: FontWeight.w800,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
