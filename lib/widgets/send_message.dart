/* 

Copyright (C) 2022 landscape contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:landscape/api/client_handler.dart';
import 'package:phosphor_flutter/phosphor_flutter.dart';
import 'dart:math' as math;

class SendMessage extends StatefulWidget {
  final ClientHandler clientHandler;
  final int roomIndex;
  const SendMessage({
    Key? key,
    required this.clientHandler,
    required this.roomIndex,
  }) : super(key: key);

  @override
  SendMessageState createState() => SendMessageState();
}

class SendMessageState extends State<SendMessage>
    with TickerProviderStateMixin {
  bool _isHovering = false;
  Animation? forwardAnimation, rotateAnimation, scaleAnimation;
  AnimationController? forwardAnimationController,
      rotateAnimationController,
      scaleAnimationController;
  final TextEditingController textEditingController = TextEditingController();

  void _send() {
    widget.clientHandler.client.rooms[widget.roomIndex].sendTextEvent(
      textEditingController.text,
    );
    textEditingController.clear();
  }

  @override
  void initState() {
    super.initState();

    forwardAnimationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    rotateAnimationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    scaleAnimationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    forwardAnimation = Tween<double>(begin: 0, end: 128).animate(
      CurvedAnimation(
        parent: forwardAnimationController!,
        curve: Curves.easeInOutSine,
      ),
    )..addListener(
        () {
          setState(
            () {},
          );
        },
      );

    rotateAnimation = Tween<double>(begin: 0, end: 45).animate(
      CurvedAnimation(
        parent: rotateAnimationController!,
        curve: Curves.easeInOut,
      ),
    )..addListener(
        () {
          setState(
            () {},
          );
        },
      );

    scaleAnimation = Tween<double>(begin: 24, end: 0).animate(
      CurvedAnimation(
        parent: scaleAnimationController!,
        curve: Curves.easeInOut,
      ),
    )..addListener(
        () {
          setState(
            () {},
          );
        },
      );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 48,
            margin: const EdgeInsets.only(
              left: 32,
              top: 32,
              bottom: 32,
            ),
            padding: const EdgeInsets.only(left: 16, right: 16, bottom: 5),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.secondary,
              borderRadius: BorderRadius.circular(50),
            ),
            child: TextField(
              onSubmitted: (value) => _send(),
              cursorColor: Colors.white,
              controller: textEditingController,
              decoration: const InputDecoration(
                hintText: 'Type a message',
                border: InputBorder.none,
              ),
            ),
          ),
        ),
        AnimatedContainer(
          duration: const Duration(milliseconds: 90),
          height: 48,
          width: 48,
          margin: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: _isHovering
                ? Theme.of(context).colorScheme.tertiary.withAlpha(127)
                : Theme.of(context).colorScheme.tertiary,
          ),
          child: GestureDetector(
            onTap: () {
              _send();
              rotateAnimationController!.forward().whenCompleteOrCancel(
                () {
                  forwardAnimationController!.forward().whenCompleteOrCancel(
                    () {
                      scaleAnimationController!.reverse();
                      forwardAnimationController!.reset();
                      rotateAnimationController!.reverse();
                    },
                  );
                  scaleAnimationController!.forward();
                },
              );
            },
            child: MouseRegion(
              hitTestBehavior: HitTestBehavior.opaque,
              onHover: (event) {
                setState(
                  () {
                    _isHovering = true;
                  },
                );
              },
              onExit: (event) {
                setState(
                  () {
                    _isHovering = false;
                  },
                );
              },
              child: ClipOval(
                child: Container(
                  margin: EdgeInsets.only(
                    left: (forwardAnimation!.value as double).clamp(0, 1024),
                  ),
                  child: Transform.rotate(
                    angle: rotateAnimation!.value * math.pi / 180,
                    child: Icon(
                      PhosphorIcons.paperPlaneTiltFill,
                      color: _isHovering
                          ? Theme.of(context).colorScheme.onTertiary
                          : Theme.of(context).colorScheme.secondary,
                      size: scaleAnimation!.value,
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
