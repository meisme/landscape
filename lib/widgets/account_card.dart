/* 

Copyright (C) 2022 landscape contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AccountCard extends StatefulWidget {
  final String name, lastMessage;
  final Image? profileImage;
  const AccountCard({
    Key? key,
    required this.name,
    required this.lastMessage,
    this.profileImage,
  }) : super(key: key);

  @override
  AccountCardState createState() => AccountCardState();
}

class AccountCardState extends State<AccountCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        left: 32,
        right: 32,
        top: 0,
        bottom: 32,
      ),
      padding: const EdgeInsets.only(
        top: 24,
        bottom: 24,
        left: 32,
        right: 16,
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.tertiary.withAlpha(48),
        borderRadius: BorderRadius.circular(33),
      ),
      child: Row(
        children: [
          CircleAvatar(
            radius: 32,
            backgroundColor: Theme.of(context).colorScheme.secondary,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(
                  left: 16,
                ),
                child: Text(
                  widget.name,
                  style: GoogleFonts.openSans(
                    color: Theme.of(context).colorScheme.onSecondary,
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                  left: 16,
                ),
                child: Text(
                  widget.lastMessage,
                  style: GoogleFonts.openSans(
                    color: Theme.of(context).colorScheme.onSecondary,
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
