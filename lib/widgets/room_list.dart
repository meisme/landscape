/* 

Copyright (C) 2022 landscape contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:matrix/matrix.dart';
import 'package:cached_network_image/cached_network_image.dart';

class RoomList extends StatefulWidget {
  final Client client;
  final Function() onRoomSelected;
  bool isReady = false;
  RoomList({
    Key? key,
    required this.client,
    required this.onRoomSelected,
  }) : super(key: key);
  int selectedIndex = 0;

  @override
  RoomListState createState() => RoomListState();
}

class RoomListState extends State<RoomList> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget.client.onSync.stream,
      builder: (context, _) => ListView.builder(
        itemCount: widget.client.rooms.length,
        itemBuilder: (context, i) => AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeInOutSine,
          margin: const EdgeInsets.only(
            right: 32,
            left: 24,
          ),
          padding: const EdgeInsets.only(
            top: 24,
            bottom: 24,
            left: 24,
            right: 24,
          ),
          decoration: BoxDecoration(
            color: widget.selectedIndex == i
                ? Theme.of(context)
                    .colorScheme
                    .tertiary
                    .withRed(28)
                    .withGreen(68)
                    .withBlue(72)
                : Theme.of(context).colorScheme.secondary,
            borderRadius: BorderRadius.circular(33),
          ),
          clipBehavior: Clip.antiAlias,
          child: MouseRegion(
            child: GestureDetector(
              onTap: (() {
                setState(() {
                  widget.selectedIndex = i;
                });
                widget.onRoomSelected();
              }),
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 32,
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    child: ClipOval(
                      child: CachedNetworkImage(
                        imageUrl: widget.client.rooms[i].avatar == null
                            ? "invalid"
                            : widget.client.rooms[i].avatar!
                                .getThumbnail(widget.client,
                                    width: 128, height: 128)
                                .toString(),
                        errorWidget: (context, url, error) => Text(
                          widget.client.rooms[i].displayname.substring(0, 1),
                          style: GoogleFonts.openSans(
                            color: Theme.of(context).colorScheme.onSecondary,
                            fontSize: 24,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(
                          left: 16,
                        ),
                        child: SizedBox(
                          width: 288,
                          child: Text(
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            widget.client.rooms[i].displayname,
                            style: GoogleFonts.openSans(
                              color: Theme.of(context).colorScheme.onSecondary,
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                          left: 16,
                        ),
                        child: SizedBox(
                          width: 320,
                          child: Text(
                            softWrap: false,
                            overflow: TextOverflow.fade,
                            widget.client.rooms[i].lastEvent?.body
                                    .replaceAll("\n", " ") ??
                                "No recent messages",
                            style: GoogleFonts.openSans(
                              color: Theme.of(context).colorScheme.onSecondary,
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
