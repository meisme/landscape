# landscape
A Matrix client in early development, written in Dart.

# Goals
- Multi-platform support
- Calls and video calls
- Spaces support
- Pinning rooms or DMs
- Viewing shared files in a dedicated view